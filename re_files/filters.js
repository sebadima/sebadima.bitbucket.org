jQuery(document).ready(function ($) {

  var bbx_cookies = Cookies.noConflict();
  var $properties = $('#properties');
  var $show_filters = $('#show-filters');
  var $show_properties = $('#show-properties');

  $show_filters.click( function() {
    $('html').addClass('show-filters');
  });

  $show_properties.click( function() {
    $('html').removeClass('show-filters');
  });

  // if (window.matchMedia('(min-width: 768px)').matches) {

  //   var $filters = $('#filters');
  //   var $footer = $('#footer');

  //   $filters.affix({
  //     offset: {
  //       top: $filters.offset().top - 65
  //     }
  //   });

  // }

  $(window).load(function() {

    var $property_items = $('.property');
    var $search = $('#search');
    var $firsts = $('.first');
    var $types = $('.filter-types .dropdown-menu a');
    var $current_type = $('#current-type');
    var $periods = $('.filter-periods .dropdown-menu a');
    var $current_period = $('#current-period');
    var $buttons = $('.btn-list .btn');
    var $price = $('#price');
    var $price_range = $('#price-range');
    var $price_range_min = $('#price-range-min');
    var $price_range_max = $('#price-range-max');
    var $count = $('#count');
    var $no_result = $('#no-result');
    var $reset = $('.reset');
    var $views = $('#view a');
    var $views_container = $('#properties-list');
    var current_view = bbx_cookies.get('properties_view') ? bbx_cookies.get('properties_view') : 'grid';
    var filters = ['', '', '', ''];
    var query = '';
    var MIN = 50000;
    var MAX = 5000000;
    var min = MIN;
    var max = MAX;

    function Thousands(number) {
      return (''+number).replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }

    function Reset() {
      filters = ['', '', '', ''];
      query = '';
      min = MIN;
      max = MAX;
      $firsts.trigger('click');
      $price.slider('option', 'values', [min, max]);
      $price_range_min.text(Thousands(min) + ' €');
      $price_range_max.text(Thousands(max) + ' €');
      $search.val('');
      Filter();
    }

    function Filter() {
      console.log(filters);
      var $results;
      var selectors = '';
      selectors += (filters[0] == '') ? '' : '.rooms-' + filters[0];
      selectors += (filters[1] == '') ? '' : '.bedrooms-' + filters[1];
      selectors += (filters[2] == '') ? '' : '.type-' + filters[2];
      selectors += (filters[3] == '') ? '' : '.period-' + filters[3];
      $property_items.hide();
      // Price
      $results = $property_items
      .filter(function() {
        var price = parseInt($(this).data('price'));
        if ((price >= min) && (price <= max)) {
          return true;
        }
      });
      // Classes
      if (selectors !== '') {
        $results = $results.filter(selectors);
      }
      // Search
      if (query !== '') {
        $results = $results.filter(function() {
          var content = $(this).find('.name').text().toLowerCase();
          content += $(this).find('.location').text().toLowerCase();
          content += $(this).find('.reference').text().toLowerCase();
          if (content.indexOf(query) > -1) {
            return true;
          } else {
            return false;
          }
        });
      }
      // Result
      $count.text($results.length);
      if ($results.length == 0) {
        $no_result.show();
      } else {
        $results.show();
        $no_result.hide();
      }
      console.log(selectors);
    }

    function View(view) {
      $views.removeClass('active');
      $views_container.removeClass(current_view);
      current_view = view;
      bbx_cookies.set('properties_view', current_view, { expires: 30, path: '/' });
      $views.parent().find('[data-view='+current_view+']').addClass('active');
      $views_container.addClass(current_view);
    }

    $views.click( function() {
      View($(this).data('view'));
    });

    View(current_view);

    // Filter();

    // $types.click(function() {
    //   var filter_index = $(this).parent().parent().data('index');
    //   var value = $(this).parent().data('value');
    //   var name = $(this).text();
    //   $current_type.text(name);
    //   filters[filter_index] = value.toString();
    //   Filter();
    //   return false;
    // });

    $periods.click(function() {
      var filter_index = $(this).parent().parent().data('index');
      var value = $(this).parent().data('value');
      var name = $(this).text();
      $current_period.text(name);
      filters[filter_index] = value.toString();
      Filter();
      return false;
    });

    $buttons.click(function() {
      $(this).parent().siblings().children().removeClass('active');
      $(this).addClass('active');
      var filter_index = $(this).parent().parent().data('index');
      var value = $(this).data('value');
      filters[filter_index] = value.toString();
      Filter();
    });

    $price.slider({
      range: true,
      min: MIN,
      max: MAX,
      step: 50000,
      values: [MIN, MAX],
      slide: function( event, ui ) {
        var a = Thousands(ui.values[0]);
        var b = Thousands(ui.values[1]);
        $price_range_min.text(a + ' €');
        $price_range_max.text(b + ' €');
        min = ui.values[0];
        max = ui.values[1];
        Filter();
      }
    });

    $search.keyup(function() {
      query = $(this).val().toLowerCase();
      Filter();
    });

    $reset.click(function() {
      Reset();
    });

  });

});
