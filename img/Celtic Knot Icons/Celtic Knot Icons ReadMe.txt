This is a set of 71 icons (11 designs in 6 colour schemes, plus some extras). The knot designs are ones I found in various places -- if any of them are yours, let me know and I'll credit you.

These are .ico files, layered in 128, 64, 48, 32, and 16 square pixel formats, 32-bit colour.

The icons are totally free for private, non-profit and non-commercial use; for any other uses, please contact me for permission. You may redistribute them as long as you include this ReadMe file, don't alter the files, and don't take credit for making them. Thank you.

Catherine Prickett
shiftercat@gmail.com
http://shiftercat.deviantart.com/